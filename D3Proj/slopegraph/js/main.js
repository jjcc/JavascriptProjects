
var margin = {top: 90, right: 40, bottom: 20, left: 200},
    width = 880 - margin.left - margin.right,
    height = 900 - margin.top - margin.bottom;

var dimensions = [];

var linex;
var projection,ordinal_labels,hover ;
var val = "";

var line = d3.line()
    .defined(function(d) { return !isNaN(d[1]); })
    .curve(d3.curveCardinal);


var yAxis = d3.axisLeft().ticks(0);

var svg = d3.select("#main").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    // Define the div for the tooltip
var div = d3.select("#main").append("div")	
    .attr("class", "tooltip")				
    .style("opacity", 0);

//d3.json("payratio2m.json").then(function(mydata) {
d3.json("data/orderchart_s0_l61.json").then(function(mydata) {

    keys =  Object.keys(mydata);
    key_len = keys.length;
    for (i= 0;i<keys.length;i++){
        dimensions.push(
          {
            name: 'place_holder',
            scale: d3.scaleLinear().range([0, height]),
            type: "number"
          },
        )
    }



  var ratios = [];
      
   keys.forEach(
     function(k,idx){
          mydata[k].forEach(function(d) {
            d.key = k;
            d.count = idx;
            ratios.push(d);
          });
          dimensions[idx].name = k;
     }
   )
  var line_domain = dimensions.map(function(d) { return d.name; }); 
  linex = d3.scalePoint()
      .domain(line_domain)
      .range([0, width]);
  var dimension = svg.selectAll(".dimension")
      .data(dimensions)
      .enter().append("g")
      .attr("class", "dimension")
      .attr("transform", function(d) { return "translate(" + linex(d.name) + ")"; });
      
      var nestedByName = d3.nest()
      	.key(function(d) { return d.name })
      	.entries(ratios);
      
      var data = [];
      nestedByName.forEach(function(d){
        var obj = {};
        obj.name = d.key;
        obj.symbol = d.values[0].symbol;
        obj.extra = d.values[0].extra;
        d.values.forEach(function(v){
          var key = v.key;//"20"+v.key.substring(1,3);
          //#obj[key] = v.rank;
          obj[key] = v.rank;
        })
        data.push(obj)
      })
    console.log(data)
  dimensions.forEach(function(dimension) {
    //dimension.scale.domain(dimension.type === "number"
    dimension.scale.domain(
         d3.extent(data, function(d) { 
           //#return +d[dimension.name].rank;}) 
           return +d[dimension.name];}) 
        //? d3.extent(data, function(d) { 
        //    return +d[dimension.name]; })
        //: data.map(function(d) { return d[dimension.name]; }).sort()
        );
  });

  dimension.append("g")
      .attr("class", "axis")
      .each(function(d) { d3.select(this).call(yAxis.scale(d.scale)); });


  svg.append("g")
      .attr("class", "background")
    .selectAll("path")
      .data(data)
    .enter().append("path")
      .attr("d", draw);

  svg.append("g")
      .attr("class", "foreground")
    .selectAll("path")
      .data(data)
    .enter().append("path")
      .attr("d", draw);
 
 svg.append("g")
    .selectAll("label")
      .data(data)
    .enter().append("text")
      .attr("id", function(d){ return d.name.replace(/[\W_]/g, '');})
      .attr("x", function(d) { return d[dimensions[0].name]? -40: get_col(d,key_len)*width/(keys.length-1) -30})
      .attr("y", function(d) { return d[dimensions[0].name]?
              dimensions[0].scale(d[dimensions[0].name]):dimensions[0].scale(d[dimensions[get_col(d,key_len)].name])})
      .attr("text-anchor", "end")
      .attr("class", "label")
      .text(function(d){ return d.name;});
hover = "line";

var xpos = width/(dimensions.length-1);
for(i=0;i<data.length;i++){
  var row = data[i];
  var last = false;
  for(j=0;j<dimensions.length;j++){
    if (j == dimensions.length -1)
      last = true;
    var circles = svg.append("circle")
                .attr("id", function(d){ return "cir"+ row.name.replace(/[\W_]/g, '');})
                 .attr("r", 3)
                 .attr("cx", j*xpos)
                 .attr("cy", function(d) { return dimensions[j].scale(row[dimensions[j].name])})
                 .style("stroke", "steelblue")
                 .style("stroke-width", 2)
                 .style("fill", "white")
                 //.attr("class", "circle")
                 .attr("class", function(d) { return last? "circle last":"circle"})
                 .on("mouseover", function(d, i){
                   hover = "circle";
                    var id = d3.select(this).attr("id");
                    var nam = id.replace("cir", '')
                    var fdata = data.filter(function(d){ return nam==d.name.replace(/[\W_]/g, '');});
                    val = dimensions[0].scale.invert(d3.mouse(this)[1]).toFixed(2);
                    //val = Math.round(val);
                    mouseover(fdata[0])     
                 })
                  .on("mouseout", mouseout);;

   var text = svg.append("text")
                  .attr("id", function(d){ return "cirt"+row.name.replace(/[\W_]/g, '');})
                 .attr("x", j*xpos-8)
                 .attr("y", function(d) { return dimensions[j].scale(row[dimensions[j].name])})
                 .attr("text-anchor", "end")
                 .attr("class", "circletext")
                 .text(function(d){ return row[dimensions[j].name]});
  }
}
dimensions.forEach(function(d, j){
  svg.append("text")
                 .attr("x", j*xpos)
                 .attr("y", -15)
                 .attr("text-anchor", "middle")
                 .attr("class", "title")
                 .text(d.name);
})
  ordinal_labels = svg.selectAll(".axis text")
      .on("mouseover", mouseover)
      .on("mouseout", mouseout);

  projection = svg.selectAll(".background path,.foreground path")
      .on("mouseover", mouseover)
      .on("mouseout", mouseout);

//
  var lastc = d3.selectAll('circle.last');

  lastc.each(function(d){
      var id = d3.select(this).attr("id");
      var nam = id.replace("cir", '')
      var fdata = data.filter(function(d){ return nam==d.name.replace(/[\W_]/g, '');});
      //val = dimensions[0].scale.invert(d3.mouse(this)[1]).toFixed(2);
      val = "hehe";
      fdata[0]['extra']['mock'] = true;
      mouseover(fdata[0])     
      sleep(200); 
  
  })

});


function mouseover(d) {
  svg.classed("active", true);
  // this could be more elegant
  if (typeof d === "string") {
    projection.classed("inactive", function(p) { return p.name !== d; });
    projection.filter(function(p) { return p.name === d; }).each(moveToFront);
    ordinal_labels.classed("inactive", function(p) { return p !== d; });
    ordinal_labels.filter(function(p) { return p === d; }).each(moveToFront);
  } else {
    projection.classed("inactive", function(p) { return p !== d; });
    projection.filter(function(p) { return p === d; }).each(moveToFront);
    ordinal_labels.classed("inactive", function(p) { return p !== d.name; });
    ordinal_labels.filter(function(p) { return p === d.name; }).each(moveToFront);
  }
  var selected = d.name.replace(/[\W_]/g, '');
  d3.selectAll(".label").style("opacity", 0.3);
  d3.selectAll(".circle").style("opacity", 0.3);
  d3.selectAll(".circletext").style("opacity", 0.3);
  if(selected == "0x")
    selected = "zerox";
  d3.select(".label#"+selected).style("opacity", 1);
  d3.selectAll(".circle#cir"+selected).style("opacity", 1);
  d3.selectAll(".circletext#cirt"+selected).style("opacity", 1);
  if(hover!="circle"){
    if(d['extra']['mock'])
      val="hehe2";
    else
      val = dimensions[0].scale.invert(d3.mouse(this)[1]).toFixed(2) + d.symbol;
  }
  
  div.transition()		
              .duration(200)		
              .style("opacity", .9);
  if(d.extra.mock)
    return
  div.html(d.name + "<br/>"+ val)	
              .style("left", (d3.event.pageX) + "px")		
              .style("top", (d3.event.pageY - 28) + "px");
}

function mouseout(d) {
  svg.classed("active", false);
  projection.classed("inactive", false);
  ordinal_labels.classed("inactive", false);
  d3.selectAll(".label").style("opacity", 1);
  d3.selectAll(".circle").style("opacity", 1);
  d3.selectAll(".circletext").style("opacity", 1);
  div.transition()		
              .duration(500)		
              .style("opacity", 0);
  if(hover=="circle"){
    hover = "line";
  }
}

function moveToFront() {
  this.parentNode.appendChild(this);
}

function draw(d) {
  return line(dimensions.map(function(dimension) {
    return [linex(dimension.name), dimension.scale(d[dimension.name])];
  }));
}

function get_col(d,limit){
  for (i=0;i< limit;i++)
    if(d[dimensions[i].name])
      return i;
  return null;
}

function sleep(ms) {
   var now = new Date().getTime();
   while(new Date().getTime() < now + ms){ /* do nothing */ } 
}
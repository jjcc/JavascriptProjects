/**
*
**/
function getGroupText(d, d3ver){
	var returnString = ""
	//console.log("in group text:" + d.name);
	var type = d3ver> 3? d.data.type: d.type;
	if(type == 'cls'){
		var children = d.children;
		d.children.map(function f(i){
			returnString +="<div class='stkname'>" + (d3ver> 3? i.data.name: i.name) +
			 "</div><div class='stkname'>(" + (d3ver > 3? i.data.code: i.code) + 
			 ")</div><div class='stkchange'>" + (d3ver >3? i.data.change: i.change) + "</div><div style='clear:both'></div>\n"; 
			});

	}
	//console.log("combined div string:" + returnString);
	return returnString;//"place holder for g";
}

function getFontSize(area,dy){
	var size = Math.min(40, 0.25*Math.sqrt(area));  
	var rsize = (size > 7 && dy > 12) ? size : 7;
	//console.log("getFontSize:" + rsize);
	return rsize;
}


function getAfterZoomContent(d){
		var returnString = "<div style='width:50px;'><a href='http://www.google.com' target='_blank'>" + d.data.name + "</a></div>";
		returnString += "<div style='width:50px;'>(" + d.data.code + ")</div>";
		returnString += "<div style='width:50px;'>H:" + (d.y1-d.y0) + "</div>";
		returnString += "<div style='width:50px;'>w:" + (d.x1-d.x0) + "</div>";
		return returnString;    
}